package pl.ciesek.cells;

import java.util.List;
import pl.ciesek.organelles.AnimalOrganelle;

public class AnimalCell implements Cell {

  public List<AnimalOrganelle> organelles;

  public AnimalCell(List<AnimalOrganelle> organelles) {
    this.organelles = organelles;
  }

  @Override
  public String toString() {
    return "AnimalCell{" +
        "organelles=" + organelles +
        '}';
  }
}
