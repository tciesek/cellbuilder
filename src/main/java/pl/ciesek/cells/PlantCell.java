package pl.ciesek.cells;

import java.util.List;
import pl.ciesek.organelles.PlantOrganelle;

public class PlantCell implements Cell {

  public List<PlantOrganelle> organelles;

  public PlantCell(List<PlantOrganelle> organelles) {
    this.organelles = organelles;
  }

  @Override
  public String toString() {
    return "PlantCell{" +
        "organelles=" + organelles +
        '}';
  }
}
