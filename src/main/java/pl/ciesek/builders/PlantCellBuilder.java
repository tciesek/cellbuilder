package pl.ciesek.builders;

import pl.ciesek.cells.PlantCell;
import pl.ciesek.organelles.PlantOrganelle;

public class PlantCellBuilder extends CellBuilder<PlantOrganelle, PlantCell> {

  // override to be able to fluently call append -> build
  // https://en.wikipedia.org/wiki/Fluent_interface
  @Override
  public PlantCellBuilder appendOrganelle(PlantOrganelle organelle) {
    super.appendOrganelle(organelle);
    return this;
  }

  @Override
  public PlantCell build() {
    return new PlantCell(getOrganelles());
  }
}
