package pl.ciesek.builders;

import pl.ciesek.cells.AnimalCell;
import pl.ciesek.organelles.AnimalOrganelle;

public class AnimalCellBuilder extends CellBuilder<AnimalOrganelle, AnimalCell> {

  // override to be able to fluently call append -> build
  // https://en.wikipedia.org/wiki/Fluent_interface
  @Override
  public AnimalCellBuilder appendOrganelle(AnimalOrganelle organelle) {
    super.appendOrganelle(organelle);
    return this;
  }

  public AnimalCell build() {
    return new AnimalCell(getOrganelles());
  }
}
