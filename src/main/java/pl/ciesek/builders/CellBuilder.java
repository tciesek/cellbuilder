package pl.ciesek.builders;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import pl.ciesek.cells.Cell;
import pl.ciesek.organelles.CellNucleus;
import pl.ciesek.organelles.CellWall;
import pl.ciesek.organelles.Organelle;

public abstract class CellBuilder<T extends Organelle, C extends Cell> {
  private List<T> organelles = new ArrayList<>();
  private List<Predicate<Organelle>> restrictions = new ArrayList<>();

  public CellBuilder() {
    restrictions.add(organelle ->
        organelle instanceof CellNucleus &&
            organelles.stream().anyMatch(CellNucleus.class::isInstance)
    );
    restrictions.add(organelle ->
        organelle instanceof CellWall &&
            organelles.stream().anyMatch(CellWall.class::isInstance)
    );
  }

  boolean canBeAppended(T organelle) {
    return restrictions.stream().noneMatch(predicate -> predicate.test(organelle));
  }

  public CellBuilder appendOrganelle(T organelle) {
    if(!canBeAppended(organelle)) {
      throw new IllegalArgumentException(
          "Organelle " + organelle + " cannot be appended (already exists)"
      );
    }

    organelles.add(organelle);
    return this;
  }

  public abstract C build();

  List<T> getOrganelles() {
    return organelles;
  }
}
