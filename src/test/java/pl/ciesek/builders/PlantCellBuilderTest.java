package pl.ciesek.builders;

import org.junit.Assert;
import org.junit.Test;
import pl.ciesek.cells.AnimalCell;
import pl.ciesek.cells.PlantCell;
import pl.ciesek.organelles.CellNucleus;
import pl.ciesek.organelles.CellWall;
import pl.ciesek.organelles.Lyzosome;
import pl.ciesek.organelles.Vacuoles;

public class PlantCellBuilderTest {

  @Test
  public void shouldCreateAnimalCell() {
    PlantCell animalCell = new PlantCellBuilder()
        .appendOrganelle(new CellNucleus())
        .appendOrganelle(new CellWall())
        .appendOrganelle(new Lyzosome())
        .appendOrganelle(new Vacuoles())
        .build();

    Assert.assertEquals(4, animalCell.organelles.size());
    Assert.assertTrue(animalCell.toString().contains("CellNucleus"));
    Assert.assertTrue(animalCell.toString().contains("CellWall"));
    Assert.assertTrue(animalCell.toString().contains("Lyzosome"));
    Assert.assertTrue(animalCell.toString().contains("Vacuoles"));
  }

  @Test
  public void shouldCreateAnimalCellWithMultipleOrganelle() {
    PlantCell cell = new PlantCellBuilder()
        .appendOrganelle(new CellNucleus())
        .appendOrganelle(new CellWall())
        .appendOrganelle(new Lyzosome())
        .appendOrganelle(new Lyzosome())
        .appendOrganelle(new Vacuoles())
        .build();

    Assert.assertEquals(5, cell.organelles.size());
    Assert.assertTrue(cell.toString().contains("CellNucleus"));
    Assert.assertTrue(cell.toString().contains("Lyzosome"));
    Assert.assertTrue(cell.toString().replaceFirst("Lyzosome", "").contains("Lyzosome"));
    Assert.assertTrue(cell.toString().contains("Vacuoles"));
  }

  @Test
  public void shouldNotCreateAnimalCellWithMultipleNucleus() {
    try {
      new PlantCellBuilder()
          .appendOrganelle(new CellNucleus())
          .appendOrganelle(new CellNucleus())
          .appendOrganelle(new Lyzosome())
          .appendOrganelle(new Vacuoles())
          .build();

    } catch (IllegalArgumentException ex) {
      Assert.assertTrue(ex.getMessage().contains("already exists"));
    }
  }

  @Test
  public void shouldNotCreateAnimalCellWithMultipleWall() {
    try {
      new PlantCellBuilder()
          .appendOrganelle(new CellNucleus())
          .appendOrganelle(new CellWall())
          .appendOrganelle(new CellWall())
          .appendOrganelle(new Lyzosome())
          .appendOrganelle(new Vacuoles())
          .build();

    } catch (IllegalArgumentException ex) {
      Assert.assertTrue(ex.getMessage().contains("already exists"));
    }
  }
}