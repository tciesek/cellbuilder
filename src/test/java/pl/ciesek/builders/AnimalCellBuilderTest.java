package pl.ciesek.builders;

import org.junit.Assert;
import org.junit.Test;
import pl.ciesek.cells.AnimalCell;
import pl.ciesek.organelles.CellNucleus;
import pl.ciesek.organelles.Lyzosome;
import pl.ciesek.organelles.Vacuoles;

public class AnimalCellBuilderTest {

  @Test
  public void shouldCreateAnimalCell() {
    AnimalCell cell = new AnimalCellBuilder()
        .appendOrganelle(new CellNucleus())
        .appendOrganelle(new Lyzosome())
        .appendOrganelle(new Vacuoles())
        .build();

    Assert.assertEquals(3, cell.organelles.size());
    Assert.assertTrue(cell.toString().contains("CellNucleus"));
    Assert.assertTrue(cell.toString().contains("Lyzosome"));
    Assert.assertTrue(cell.toString().contains("Vacuoles"));
  }

  @Test
  public void shouldNotCreateAnimalCellWithMultipleNucleus() {
    try {
      new AnimalCellBuilder()
          .appendOrganelle(new CellNucleus())
          .appendOrganelle(new CellNucleus())
          .appendOrganelle(new Lyzosome())
          .appendOrganelle(new Vacuoles())
          .build();

    } catch (IllegalArgumentException ex) {
      Assert.assertTrue(ex.getMessage().contains("already exists"));
    }
  }

  @Test
  public void shouldCreateAnimalCellWithMultipleOrganelle() {
    AnimalCell cell = new AnimalCellBuilder()
        .appendOrganelle(new CellNucleus())
        .appendOrganelle(new Lyzosome())
        .appendOrganelle(new Lyzosome())
        .appendOrganelle(new Vacuoles())
        .build();

    Assert.assertEquals(4, cell.organelles.size());
    Assert.assertTrue(cell.toString().contains("CellNucleus"));
    Assert.assertTrue(cell.toString().contains("Lyzosome"));
    Assert.assertTrue(cell.toString().replaceFirst("Lyzosome", "").contains("Lyzosome"));
    Assert.assertTrue(cell.toString().contains("Vacuoles"));
  }

}